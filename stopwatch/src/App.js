import Stopwatch from './component/Stopwatch';
import './App.css'

function App() {
  return (
    <div className="container">
      <Stopwatch />
    </div>
  );
}

export default App;
