import React from "react";
import "./Stopwatch.css";

class Stopwatch extends React.Component {
  state = {
    seconds: 0,
  };

  start() {
    if (this.interval) return;

    this.interval = setInterval(() => {
      this.setState({ seconds: this.state.seconds + 1 });
    }, 1000);
  }

  stop() {
    clearInterval(this.interval);
    this.interval = null;
  }

  reset() {
    this.stop();
    this.setState({ seconds: 0 });
  }

  render() {
    const seconds = this.state.seconds % 60;
    const minutes = Math.floor(this.state.seconds / 60) % 60;
    const hours = Math.floor(this.state.seconds / 3600);

    return (
      <div className="stopwatch">
        <div className="display">
          <p>
            {hours} : {minutes} : {seconds}
          </p>
          <p>Jam : Menit : Detik</p>
        </div>

        <div className="buttons">
          <button className="reset" onClick={this.reset.bind(this)}>
            Reset
          </button>
          <button className="start" onClick={this.start.bind(this)}>
            Start
          </button>
          <button className="stop" onClick={this.stop.bind(this)}>
            Stop
          </button>
        </div>
      </div>
    );
  }
}

export default Stopwatch;
